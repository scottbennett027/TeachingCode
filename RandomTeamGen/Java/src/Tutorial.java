// Written by Scott Bennett, scottbennett027@gmail.com
// A simple java program to randomly choose list elements

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Tutorial {

	final static int maxTeamSize = 2;
	
	public static void main(String[] args) 
	{
		// Make the array and add the possible players
		List<String> players = new ArrayList<>();
		players.add("Player 1");
		players.add("Player 2");
		players.add("Player 3");
		players.add("Player 4");
		players.add("Player 5");

		System.out.println("The players are: " + players);
		
		// Seed a random number generator
		Random rng = new Random();
		int randomIndex;
		
		// The new teams
		List<String> teamA = new ArrayList<>();
		List<String> teamB = new ArrayList<>();
		
		// Put the possible players on teams randomly
		while( teamA.size() < maxTeamSize)
		{
			randomIndex = rng.nextInt(players.size());
			teamA.add(players.get(randomIndex));
			players.remove(randomIndex);
		}
		
		while( teamB.size() < maxTeamSize)
		{
			randomIndex = rng.nextInt(players.size());
			teamB.add(players.get(randomIndex));
			players.remove(randomIndex);
		}
		
		// Print the results
		System.out.println("Team A: " + teamA);
		System.out.println("Team B: " + teamB);
		System.out.println("Players not on teams: " + players);
		System.out.println("\nHappy Coding! :)");
	}

}
