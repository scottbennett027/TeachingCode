# Written by Scott Bennett, scottbennett027@gmail.com
# A simple Ruby program to randomly chose array elements and sort them into 2 groups
# NOTE: I don't actually know how to code in Ruby, this is the result of Googling everything
#       and I almost guarentee there is a better way to do most of this

# Constant for the number of players on a team
PLAYERS_PER_TEAM = 2

# Initialize a list of players that can be chosen
players = [ "Player 1","Player 2","Player 3","Player 4","Player 5" ]
puts "The players are: " + players.join( ', ' )
puts ""

# Empty Arrays for the new teams
teamA = []
teamB = []


playersDrafted = 0
# While 2 players haven't been drafted to team A, draft one
while( playersDrafted < PLAYERS_PER_TEAM )
    #Take a random player from the player pool
    teamA.push( players.sample )
    #increment our counter
    playersDrafted+=1

    # Do a logic subtraction, removing the player from the pool that was chosen
    # ie. ['Player 1', 'Player 2'] - ['Player 1'] = ['Player 2']
    players = players - teamA
end

# The same as above for the second team
playersDrafted = 0
while(playersDrafted < PLAYERS_PER_TEAM)
    teamB.push( players.sample )
    playersDrafted += 1
    players = players - teamB
end

# Print the results
puts "The players on Team A are: " + teamA.join( ', ' )
puts "The players on Team B are: " + teamB.join( ', ' )
puts "Players not on a team: " + players.join( ' ' )
puts ""
puts "Happy Coding! :)"