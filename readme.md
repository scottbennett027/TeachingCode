# TEACHING CODE
All code is written by Scott Bennett, unless otherwise stated in the source files.

A Github repo of basic code for teaching purposes. Code should all be well commented. 

If it isn't contact me: scottbennett027@gmail.com

__Programs__

1. RandomTeamGen - a simple program to randomly assign players to teams

2. GameHub - a program that allows multiple different types of games to be played
