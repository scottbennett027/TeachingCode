import GameUtils
import time, random, math

def playManhunt():

    MAX_ROWS = 6
    MAX_COLS = 6
    PLACE_SEARCHED = "^"
    UP = "w"
    DOWN = "s"
    LEFT = "a"
    RIGHT = "d"

    print("Welcome to Manhunt!!!!\nThe player can choose the starting location,\
and a target is placed randomly in the map. The player then can use (wasd) \
to search for the target.")

    # Get and verify valid rows and columns
    rows = int(input("How many rows should the board have? "))
    while ( not GameUtils.validNumberInput(rows, 1, MAX_ROWS) ):
        rows = int(input("How many rows should the board have? "))

    cols = int(input("How many columns should the board have? "))
    while ( not GameUtils.validNumberInput(cols, 1, MAX_COLS) ):
        cols = int(input("How many rows should the board have? "))

    # Make a board and choose player start
    manhuntBoard = GameUtils.makeGameBoard(rows, cols, "*")
    MAX_MOVES = math.floor( (rows*cols)*(2/3) )
    
    playerRow = int( input( "\nIn what row would you like to start?? " ) )
    while ( not GameUtils.validNumberInput(playerRow, 1, MAX_ROWS) ):
        playerRow = int( input( "\nIn what row would you like to start?? " ) )
    playerCol = int( input( "\nIn what column would you like to start?? " ) )
    while ( not GameUtils.validNumberInput(playerCol, 1, MAX_ROWS) ):
        playerCol = int( input( "\nIn what column would you like to start?? " ) )

    playerMark = input( "\nWhat would you like to use as your marker? One character only please! " )
    while len(playerMark) > 1:
        print( "That input was too long, please try again" )
        playerMark = input( "\nWhat would you like to use as your marker? One character only please! " )
    
    GameUtils.clearScreen()
    print( "This is your gameboard:\n" )
    playerRow -= 1
    rows -= 1
    playerCol -= 1
    cols -= 1
    manhuntBoard[playerRow][playerCol] = playerMark
    GameUtils.printBoard(manhuntBoard)

    # Get target location and check that it isn't at the players location
    targetRow = random.randint(0,rows)
    targetCol = random.randint(0,cols)
    while ( (targetRow, targetCol) == (playerRow, playerCol) ):
        targetRow = random.randint(0,rows)
        targetCol = random.randint(0,cols)

    print("Target Location: " + str((targetRow, targetCol)))
    playerMoves = 0
    playerWin = False
    print("Based on the size of the board, you have " + str(MAX_MOVES) + " moves \
to find the target")
    while ( playerMoves < MAX_MOVES ):
        playerMoves += 1

        # Take the players move and validate it
        userMove = input( "Which direction would you like to move in?\n(w -> up, a -> left, s -> down, d -> right): " )
        while ( not isValidMove(userMove, rows, cols, playerRow, playerCol) ):
            print( "That direction was invalid, try again!" )
            userMove = input( "Which direction would you like to move in?\n(w -> up, a -> left, s -> down, d -> right): " )
        
        # Do the move
        if userMove is RIGHT:
            manhuntBoard[playerRow][playerCol] = PLACE_SEARCHED
            playerCol += 1
            manhuntBoard[playerRow][playerCol] = playerMark
        
        if userMove is LEFT:
            manhuntBoard[playerRow][playerCol] = PLACE_SEARCHED
            playerCol -= 1
            manhuntBoard[playerRow][playerCol] = playerMark

        if userMove is UP:
            manhuntBoard[playerRow][playerCol] = PLACE_SEARCHED
            playerRow -= 1
            manhuntBoard[playerRow][playerCol] = playerMark

        if userMove is DOWN:
            manhuntBoard[playerRow][playerCol] = PLACE_SEARCHED
            playerRow += 1
            manhuntBoard[playerRow][playerCol] = playerMark

        # Check win
        if ( (targetRow, targetCol) == (playerRow, playerCol) ):
            playerWin = True
            break
        
        GameUtils.clearScreen();
        GameUtils.printBoard(manhuntBoard)

    if ( playerWin ):
        print("Congrats! You found the target in " + str(playerMoves) + " moves!\nThe target \
was located at " + str((targetRow, targetCol)) )
    else:
        print("You used all your moves and couldn't find the target!!\n\
The target was located at " + str((targetRow, targetCol)))

    print("\n")
    GameUtils.printBoard(manhuntBoard)
    print("\n\nReturning to Gamehub Main Menu!")
    time.sleep(5)
    GameUtils.clearScreen()

# inputVal - value from player
# rowMax - number of rows in board
# colMax - number of cols in board
# playerRow - row the player is in
# playerCol - column the player is in
def isValidMove(inputVal, rowMax, colMax, playerRow, playerCol):

    ALLOWED_MOVE = ["w", "a", "s", "d"]
    UP = "w"
    DOWN = "s"
    LEFT = "a"
    RIGHT = "d"

    # Lets check the key stroke for validity
    if ( inputVal not in ALLOWED_MOVE):
        return False

    # Check if move is in bounds
    if (inputVal is RIGHT) and (playerCol == colMax):
        return False
        
    if (inputVal is LEFT) and (playerCol == 0):
        return False

    if (inputVal is UP) and (playerRow == 0):
        return False

    if (inputVal is DOWN) and (playerRow == rowMax):
        return False
    
    return True
