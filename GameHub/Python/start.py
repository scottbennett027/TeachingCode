#!/usr/bin/env python3

# Scott Bennett, scottbennett027@gmail.com
# A tutorial program for creating a two dimensional map in python

import TicTacToe, Manhunt
import os, sys, logging, time

# Constants
NUMBER_GAMES = 2
TTT_NUMBER = 1
MANHUNT_NUMBER = 2
MAIN_MENU_EXIT = 3

# Print the main menu
def printMenu():
    print("Select the game you would like to play:\n\
1. Tic Tac Toe\n2. Manhunt\n\n3. Exit")

if __name__ == '__main__':

    print("\nWelcome to legacy games! All the oldies but goodies!!\n")
    
    # Print the menu and take the user's choice
    printMenu()
    
    try: 
        uChoice = int(input("What would you like to play? "))
        while not GameUtils.validNumberInput(uChoice, 1, MAIN_MENU_EXIT):
            uChoice = int(input("What would you like to play? "))
    except ValueError:
        print("You can't enter a blank space, dummy")
        sys.exit(-1)
    # While the user doesn't want to quit
    while not uChoice == MAIN_MENU_EXIT:

        if uChoice == TTT_NUMBER:
            TicTacToe.playTTT()
        elif uChoice == MANHUNT_NUMBER:
            Manhunt.playManhunt()
        else:
            print("This shouldn't happen")
        
        # After the game is over, go back to the main menu
        print("Thanks for playing! Taking you back to the main menu :)\n\n")
        printMenu()
        try:
            uChoice = int(input("What would you like to play? "))
            while not GameUtils.validNumberInput(uChoice, 1, MAIN_MENU_EXIT):
                uChoice = int(input("What would you like to play? "))
        except ValueError:
            print("You can't enter a blank space, dummy")
            sys.exit(-1)
    # Exit message
    print("\nThanks for playing! Come Again!\n\n")
