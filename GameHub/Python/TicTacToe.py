# Import the utilities
import GameUtils
# Other Imports
import time

# Function for playing TTT
def playTTT():

    scoreBoard = [3, 3, 3, 3, 3, 3, 3, 3]
    whereToPlay = [["1","2","3"],["4","5","6"],["7","8","9"]]

    print("Welcome to Tic Tac Toe!!!\nPlayer 1 will be 'X' \
and Player 2 will be 'O'\nPlayer 1 will go first, but let \
me make the board really quick :)")
    
    TTTBoard = GameUtils.makeGameBoard(3,3,"_")

    hasWon = False
    turn = 1
    player = 1
    while (turn < 10):
        GameUtils.clearScreen()
        print("It is Player " + str(player) + "'s turn")
        GameUtils.printBoard(TTTBoard)
        print("\n\n")
        GameUtils.printBoard(whereToPlay)
        print("\nThe above picture outlines where you can play.")
        userMove = int(input("Where would you like to play? "))
        while not (GameUtils.validNumberInput(userMove, 1, 9) and isSpaceEmpty(TTTBoard, userMove)):
            userMove = int(input("Where would you like to play? "))
        
        # Determine marker
        if (player) == 1:
            marker = "X"
        else:
            marker = "O"

        #Place marker and update/check score
        row = (userMove-1) // 3
        col = (userMove-1) % 3
        TTTBoard[row][col] = marker
        scoreBoard = updateScore(scoreBoard, userMove, player)
        hasWon, player = checkWin(scoreBoard)
        if hasWon:
            break

        # End of turn stuff
        turn += 1
        if turn % 2 == 0:
            player = 2
        else:
            player = 1
    
    if hasWon:
        print("Congrats Player " + str(player) + ", YOU HAVE WON!")
    else:
        print("That hard fought battle ended in a Tie!!")
        
    GameUtils.printBoard(TTTBoard)
    print("\nReturning to Gamehub Main Menu!")
    time.sleep(5)
    GameUtils.clearScreen()
        

def updateScore(score, userMove, playerTurn):
    if playerTurn == 1:
        modifier = 1
    else:
        modifier = -1

    # score array is [row1, row2, row3, col1, col2, col3, backslash, foward slash]
    # 1 2 3
    # 4 5 6
    # 7 8 9
    if (userMove == 1):
        score[0] += modifier
        score[3] += modifier
        score[6] += modifier
    elif (userMove == 2):
        score[0] += modifier
        score[4] += modifier
    elif (userMove == 3):
        score[0] += modifier
        score[5] += modifier
        score[7] += modifier
    elif (userMove == 4):
        score[1] += modifier
        score[3] += modifier
    elif (userMove == 5):
        score[1] += modifier
        score[4] += modifier
        score[6] += modifier
        score[7] += modifier
    elif (userMove == 6):
        score[1] += modifier
        score[5] += modifier
    elif (userMove == 7):
        score[2] += modifier
        score[3] += modifier
        score[7] += modifier
    elif (userMove == 8):
        score[2] += modifier
        score[4] += modifier
    else:
        score[2] += modifier
        score[5] += modifier
        score[6] += modifier

    return score

# A function to check if a player has won
# Returns a tuple
def checkWin(score):

    for i in range(len(score)):
        if score[i] == 6:
            return True,1
        if score[i] == 0:
            return True,2
    
    return False,None

# A function to determine whether the space on the board
# is empty or not
# board - A two dimensional array
# userMove - The space to check
# returns True if empty, False if occupied
def isSpaceEmpty(board, userMove):
    
    # Just think about it for a second
    row = (userMove-1) // 3
    col = (userMove-1) % 3
    
    # If the space is unused, return true
    if board[row][col] == "_":
        return True
    else:
        return False
