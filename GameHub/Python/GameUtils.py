import os, pygame

# Clear the Screen cause we are cool like that
def clearScreen(): 
  
    # for windows 
    if os.name == 'nt': 
        _ = os.system('cls') 
  
    # for mac and linux(here, os.name is 'posix') 
    else: 
        _ = os.system('clear')

# This function validates the user's input to make sure that
# the values fall in the specified bounds

# inputVal - The input value
# lower - The lower bound for the input value
# upper - The upper bound for the input value
def validNumberInput(inputVal, lower, upper):
    valid = False

    # If there is an upper limit but no lower limit
    if lower is None and not (upper is None):
        if inputVal > upper:
            print("The input value is too large, select a smaller value.")
        else:
            valid = True
    # If there is a lower limit but no upper limit
    elif not (lower is None) and upper is None:
        if inputVal < lower:
            print("The input value is too small, select a larger value.")
        else:
            valid = True
    # There is a limit for both
    else:
        if inputVal > upper:
            print("The input value is too large, select a smaller value.")
        elif inputVal < lower:
            print("The input value is too small, select a larger value.")
        else:
            valid = True
    return valid


######################## GENERAL BOARD OPERATIONS ##################################

# This function creates a game board from the validated user input sizes
# rows - number of rows for the board to be
# cols - numer of columns for the board to be

def makeGameBoard(rows, cols, symbol):
    
    # Game Boards are just two D lists, or a list of lists

    # gameboard is the outer list
    gameBoard = []
    for i in range(rows):
        # for each index in gameboard, make a new array
        newCol = []
        for j in range(cols):
            # Fill the array with the correct number of spaces
            newCol.append(symbol)
        # Add the new array to the current index
        gameBoard.append(newCol)

    return gameBoard

# Print the board
# board - a printable board
def printBoard(board):
    for i in range(len(board)):
        print(" ".join(board[i]))
